import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {HeroListComponent} from './hero-list/hero-list.component';
import {HeroDetailComponent} from './hero-detail/hero-detail.component';

const heroesRoutes: Routes = [
  {path: 'app3/heroes', redirectTo: '/app3/superheroes'},
  {path: 'app3/hero/:id', redirectTo: '/app3/superhero/:id'},
  {path: 'app3/superheroes', component: HeroListComponent, data: {animation: 'heroes'}},
  {path: 'app3/superhero/:id', component: HeroDetailComponent, data: {animation: 'hero'}}
];

@NgModule({
  imports: [
    RouterModule.forChild(heroesRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class HeroesRoutingModule {
}
